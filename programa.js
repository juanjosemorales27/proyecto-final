function crearLista(){


    let elemento=document.createElement("li");
    let texto=document.createTextNode("sitio Javascript");

    elemento.appendChild(texto);

    let lista=document.getElementById(listaSitios)

    lista.appendChild(elemento);


 
}
function cargarMapa1() {
    let mapa = L.map('divMapa', { center: [  4.6518474941490195,-74.17370349168777], zoom:14});

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    
    let marcadorParque= L.marker([4.6518474941490195,-74.17370349168777 ]);
    marcadorParque.addTo(mapa);

    var polygon = L.polygon([
        [4.648588644259918,-74.180588722229],
        [4.638579416368318,-74.17543888092041],
        [4.646321609360345,-74.16166305541992],
        [4.653978169639255,-74.16882991790771 ],
        [4.648588644259918,-74.180588722229]
    ]).addTo(mapa);
 
}
function cargarMapa2() {
    let mapa = L.map('divMapa', { center: [ 4.651191180638183,-74.17076244950294], zoom:14});

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    
    let marcadorPescaderia= L.marker([4.651191180638183,-74.17076244950294 ]);
    marcadorPescaderia.addTo(mapa);

    var polygon = L.polygon([
        [4.648588644259918,-74.180588722229],
        [4.638579416368318,-74.17543888092041],
        [4.646321609360345,-74.16166305541992],
        [4.653978169639255,-74.16882991790771 ],
        [4.648588644259918,-74.180588722229]
    ]).addTo(mapa);
 
}
function cargarMapa3() {
    let mapa = L.map('divMapa', { center: [ 4.648700926459273,-74.16797161102295], zoom:14});

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    
    let marcadorPescaderia= L.marker([4.648700926459273,-74.16797161102295 ]);
    marcadorPescaderia.addTo(mapa);

    var polygon = L.polygon([
        [4.648588644259918,-74.180588722229],
          [4.638579416368318,-74.17543888092041],
          [4.646321609360345,-74.16166305541992],
          [4.653978169639255,-74.16882991790771 ],
          [4.648588644259918,-74.180588722229]
    ]).addTo(mapa);
 
}
function cargarMapa4() {
    let mapa = L.map('divMapa', { center: [ 4.648700926459273,-74.16797161102295], zoom:13});

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    
    let marcadorPescaderia= L.marker([4.648700926459273,-74.16797161102295 ]);
    marcadorPescaderia.addTo(mapa);

    var polygon = L.polygon([
        [4.648588644259918,-74.180588722229],
          [4.638579416368318,-74.17543888092041],
          [4.646321609360345,-74.16166305541992],
          [4.653978169639255,-74.16882991790771 ],
          [4.648588644259918,-74.180588722229]

    ]).addTo(mapa);
 
}
function cargarMapa5() {
    let mapa = L.map('divMapa', { center: [ 4.645512907056988,-74.17159661650658], zoom:13});

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    
    let marcadorPescaderia= L.marker([  4.645512907056988,-74.17159661650658 ]);
    marcadorPescaderia.addTo(mapa);

    var polygon = L.polygon([
        [4.648588644259918,-74.180588722229],
          [4.638579416368318,-74.17543888092041],
          [4.646321609360345,-74.16166305541992],
          [4.653978169639255,-74.16882991790771 ],
          [4.648588644259918,-74.180588722229]

    ]).addTo(mapa);
 
}
function cargarMapa6() {
    let mapa = L.map('divMapa', { center: [  4.645512907056988 ,-74.17159661650658], zoom:13});

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    
    let marcadorPescaderia= L.marker(   [4.6447349479328555,-74.16843831539154 ] );
    marcadorPescaderia.addTo(mapa);

    var polygon = L.polygon([
        [4.648588644259918,-74.180588722229],
          [4.638579416368318,-74.17543888092041],
          [4.646321609360345,-74.16166305541992],
          [4.653978169639255,-74.16882991790771 ],
          [4.648588644259918,-74.180588722229]

    ]).addTo(mapa);
 
}
function guardarDatos(){
    localStorage.Organizador= document.getElementById("Organizador").value;
    localStorage.Evento= document.getElementById("Evento").value;
    localStorage.Descripcion= document.getElementById("Descripcion").value;
    localStorage.Fecha= document.getElementById("Fecha").value;
   
}
